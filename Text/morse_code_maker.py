#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest
import time
import winsound
import sys
from PyQt5.QtWidgets import (QWidget, QPushButton, QApplication, QLabel, QLineEdit, QHBoxLayout, QVBoxLayout)
from PyQt5.QtCore import Qt
import threading


class MorseCode:
    def __init__(self, cancel):
        self.cancel = cancel
        self.morsecodedict = {
            'a': '01',
            'b': '1000',
            'c': '1010',
            'd': '100',
            'e': '0',
            'f': '0010',
            'g': '110',
            'h': '0000',
            'i': '00',
            'j': '0111',
            'k': '101',
            'l': '0100',
            'm': '11',
            'n': '10',
            'o': '111',
            'p': '0110',
            'q': '1101',
            'r': '010',
            's': '000',
            't': '1',
            'u': '001',
            'v': '0001',
            'w': '011',
            'x': '1001',
            'y': '1011',
            'z': '1100',
            ' ': '2',
            '1': '01111',
            '2': '00111',
            '3': '00011',
            '4': '00001',
            '5': '00000',
            '6': '10000',
            '7': '11000',
            '8': '11100',
            '9': '11110',
            '0': '11111'
        }

    def morseCodeMaker(self, text):
        text = text.lower()
        for index, letter in enumerate(text):
            morse = self.morsecodedict[letter]
            for num in range(len(morse)):
                while not self.cancel.wait(0):
                    try:
                        inindex = morse[num:num + 1]
                    except Exception:
                        next
                    if morse[num:num + 1] == '0':
                        winsound.PlaySound('e.wav', winsound.SND_FILENAME)
                        time.sleep(.1)  # Dot
                    elif morse[num:num + 1] == '1':
                        winsound.PlaySound('e.wav', winsound.SND_FILENAME)
                        time.sleep(.3)  # Dash
                    elif morse[num:num + 1] == '2':
                        # Time to sleep between words, combines with time between letters to make .7 sec delay
                        time.sleep(.4)
                    break
            time.sleep(.3)  # Time to sleep between letters


'''
class MyTest(unittest.TestCase):
    def test_me(self):
        m = MorseCode()
        self.assertEqual(m.morseCodeMaker('SOS 1234567890'), (None)) #OK

unittest.main()
'''


class MorseCodeGUI(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.t = None

    def initUI(self):
        title = QLabel('Enter a string to be converted to morse code:')
        self.titleEdit = QLineEdit()
        self.okButton = QPushButton("OK")
        self.cancelButton = QPushButton("Clear")

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(self.okButton)
        hbox.addWidget(self.cancelButton)

        vbox = QVBoxLayout()
        vbox.addWidget(title)
        vbox.addWidget(self.titleEdit)
        vbox.addLayout(hbox)
        vbox.addStretch(1)

        self.cancelButton.clicked.connect(self.buttonClicked)
        self.okButton.clicked.connect(self.buttonClicked)

        self.setLayout(vbox)
        self.setGeometry(300, 300, 500, 50)
        self.setWindowTitle('Morse Code')
        self.show()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.cancelButton.click()
        if e.key() == Qt.Key_Return or e.key() == Qt.Key_Enter:
            self.okButton.click()

    def onChanged(self, text):
        self.kill = threading.Event()
        self.t = threading.Thread(target=MorseCode(self.kill,).morseCodeMaker, args=(text,))
        self.t.setDaemon(True)
        self.t.start()

    def buttonClicked(self):
        default = 'OK'
        sender = self.sender()
        if sender.text() == 'OK':
            if self.t is not None:
                if self.t.is_alive():
                    pass
                else:
                    self.onChanged(self.titleEdit.text())
            else:
                self.onChanged(self.titleEdit.text())
        else:
            if self.t.is_alive():
                self.kill.set()
                self.t.join()
            else:
                self.titleEdit.clear()

    def closeEvent(self, event):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MorseCodeGUI()
    sys.exit(app.exec_())
