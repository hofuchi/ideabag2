#! python3
import unittest
import sys


class Solution:
    def __init__(self):
        self.stack = []
        self.queue = []

    def pushCharacter(self, item):
        self.stack.append(item)

    def popCharacter(self):
        return self.stack.pop()

    def dequeueCharacter(self):
        return self.queue.pop()

    def enqueueCharacter(self, item):
        self.queue.insert(0, item)

    def isPalindrome(self, word):
        s = Solution()

        l = len(word)
        # push/enqueue all the characters of string s to stack
        for i in range(l):
            s.pushCharacter(word[i])
            s.enqueueCharacter(word[i])

        isPalindrome = True
        '''
        pop the top character from stack
        dequeue the first character from queue
        compare both the characters
        '''
        for i in range(l // 2):
            if s.popCharacter() != s.dequeueCharacter():
                isPalindrome = False
                break
        # finally print whether string word is palindrome or not.
        if isPalindrome:
            return ("The word, " + word + ", is a palindrome.")
        else:
            return ("The word, " + word + ", is not a palindrome.")


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.isPalindrome('racecar'), ('The word, racecar, is a palindrome.'))  # OK
        self.assertEqual(s.isPalindrome('no'), ('The word, no, is not a palindrome.'))  # OK


unittest.main()
