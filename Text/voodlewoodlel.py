#! python3
import unittest


class Voodlewoodlel:
    def __init__(self):
        pass

    def voodlewoodlel(self, string):
        splitstring = string.split('\n')
        vowellist = ['a', 'e', 'i', 'o', 'u']
        outputstring = ''
        for name in range(len(splitstring)):
            string = splitstring[name]
            addnum = 0
            for num in range(len(string)):
                num = num + addnum
                if string[num].lower() in vowellist:
                    vowel = string[num]
                    string = string[:num] + 'oodle' + string[num + 1:]
                    addnum += 4
            outputstring += string + '\n'

        return outputstring.rstrip('\n')


class MyTest(unittest.TestCase):
    def test_me(self):
        v = Voodlewoodlel()
        self.assertEqual(v.voodlewoodlel('Alice\nGraham\nDonny'),
                         ('oodleloodlecoodle\nGroodlehoodlem\nDoodlenny'))  # OK


unittest.main()
