#! python3
import unittest


class String:
    def __init__(self, string):
        self.string = string

    def halfTheString(self):
        if len(self.string) % 2 == 0:
            string = self.string[:(len(self.string) / 2)]
        else:
            return 'This string is not valid'

        return string


class MyTest(unittest.TestCase):
    def test_me(self):
        s = String('Yes')
        self.assertEqual(s.halfTheString(), ('This string is not valid'))  # OK
        s = String('No')
        self.assertEqual(s.halfTheString(), ('N'))  # OK


unittest.main()
