#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest
from collections import deque


class Crypto:
    def __init__(self):
        pass

    # VINGENERE
    # char from input string | char from key | rotation amount | result char
    # --------------------------------------------------------------------------
    # T                      | b             | 1               | U
    # h                      | o             | 14              | v
    # e                      | o             | 14              | s
    # (space)                | n/a           | n/a             | (space)
    # c                      | m             | 12              | o
    # r                      | b             | 1               | s
    # o                      | o             | 14              | c
    # w                      | o             | 14              | k
    # (and so on …)
    def vingenere(self, string=None, keyString=None):

        # Gets user input if arguments not already defined
        if string is None:
            string = input(u"Enter a string:\u0020")
        if keyString is None:
            keyString = input(u"Rotate by (string):\u0020")

        # Checks for valid input
        try:
            int(keyString)  # Checks if keyString is a plain number
            return ("# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")
        except Exception:
            if not isinstance(keyString, str):
                return ("# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")
            elif len(str(keyString)) < 1:
                return ("# vingenere(string:str, keyString:str) # <class 'ValueError'> # Enter a valid keyString")
            if not isinstance(string, str):
                return ("# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid string")
            elif len(str(string)) < 1:
                return ("# vingenere(string:str, keyString:str) # <class 'ValueError'> # Enter a valid string")

        # Match every character in string with it's corresponding keyString character
        splitKeyString = []
        splitKeyStringOrig = deque(list(keyString))
        keyLength = len(splitKeyStringOrig)
        loop = 0
        for i, char in enumerate(string):
            if len(string) >= len(splitKeyString):
                ordChar = ord(char)
                # Upper or lower case letter
                if (65 <= ordChar < 91) or (97 <= ordChar < 123):
                    if loop == 1:
                        # Handle keyString positioning for special characters
                        splitKeyString[-1] = splitKeyStringOrig[i % keyLength]
                        # Handling of special character group ends
                        loop = 0
                    else:
                        splitKeyString.append(splitKeyStringOrig[i % keyLength])
                # Handle special character group (begins/continues)
                else:
                    splitKeyString.append('f')  # 'f' for filler
                    # Next character
                    if len(string) > i + 1:
                        ordChar = ord(string[i + 1])
                    else:
                        # Next character is no more
                        break
                    # Special character grouping ends
                    if (65 <= ordChar < 91) or (97 <= ordChar < 123):
                        splitKeyString.append(splitKeyStringOrig[i % keyLength])
                        # Handle keyString positioning for special characters

                        splitKeyStringOrig.rotate(1)
                        loop = 1
                    # Two or more special characters in a row
                    else:
                        # Handle keyString positioning for special characters
                        splitKeyStringOrig.rotate(1)

        # Generate mildly encrypted returnString
        returnString = ''
        for char, keyChar in zip(string, splitKeyString):
            # Translate character into a Unicode code point
            ordChar = ord(char)
            # Adjust keyNum accordingly to correctly match uppercase keyString characters
            if keyChar.isupper():
                keyNum = abs(ord(keyChar.upper()) - 65)
            # Adjust keyNum accordingly to correctly match lowercase keyString characters
            else:
                keyNum = abs(ord(keyChar) - 97)
            # Upper case letter
            if 65 <= ordChar < 91:
                # Generate Unicode code point for new character
                newOrd = ordChar + keyNum
                while newOrd >= 91:
                    newOrd = newOrd - 26
            # Lower case letter
            elif 97 <= ordChar < 123:
                # Generate Unicode code point for new character
                newOrd = ordChar + keyNum
                while newOrd >= 123:
                    newOrd = newOrd - 26
            # Special character
            else:
                # Generate Unicode code point for new character
                newOrd = ord(char)

            # Translate Unicode code point back into a character
            newChar = chr(newOrd)
            returnString += newChar

        return returnString

    # CAESAR
    # char | rot | Return value
    # --------------------------------------------------------------------------
    # a    | 13  | n
    # a    | 14  | o
    # a    | 0   | a
    # c    | 26  | c
    # c    | 27  | d
    # A    | 13  | N
    # z    | 1   | a
    # Z    | 2   | B
    # z    | 37  | k
    # !    | 37  | !
    # 6    | 13  | 6
    def caesar(self, string=None, keyNum=None):

        # Gets user input if arguments not already defined
        if string is None:
            string = input(u"Enter a string:\u0020")
        if keyNum is None:
            keyNum = input(u"Rotate by (int):\u0020")

        # Checks for valid input
        if True:
            try:
                if str(keyNum) == 'True' or str(keyNum) == 'False':
                    raise TypeError("keyNum cannot be True or False")
                keyNum = int(keyNum)
            except ValueError:
                return ("# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")
            except TypeError:
                return ("# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")
        elif len(str(keyNum)) < 1:
            return ("# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")
        if not isinstance(string, str):
            return ("# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid string")
        elif len(str(string)) < 1:
            return ("# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid string")

        # Rotate the letters in the string keyNum places, 'a'+25 => 'z'
        returnString = ''
        for char in string:
            # Translate character into a Unicode code point
            ordChar = ord(char)
            # Uppercase letter
            if (65 <= ordChar < 91):
                # Unicode code point for uppercase `a`
                baseOrd = 65
                # Handles negative keyNums
                if keyNum < 0:
                    # Generate Unicode code point for new character
                    # `+13` from baseOrd % -26 == -13
                    newOrd = ((ordChar + keyNum + 13) % 26) + (baseOrd)
                # Handles positive keyNums
                else:
                    # Generate Unicode code point for new character
                    # `-13` from baseOrd % 26 == 13
                    newOrd = ((ordChar + keyNum - 13) % 26) + (baseOrd)
            # Lowercase letter
            elif (97 <= ordChar < 123):
                # Unicode code point for lowercase `a`
                baseOrd = 97
                # Handles negative keyNums
                if keyNum < 0:
                    # Generate Unicode code point for new character
                    # `+7` from baseOrd % -26 == -7
                    newOrd = ((ordChar + keyNum + 7) % 26) + (baseOrd)
                # Handles positive keyNums
                else:
                    # Generate Unicode code point for new character
                    # `-19` from baseOrd % 26 == 19
                    newOrd = ((ordChar + keyNum - 19) % 26) + (baseOrd)
            # Special character
            else:
                # Generate Unicode code point for new character
                newOrd = ord(char)

            # Translate Unicode code point back into a character
            newChar = chr(newOrd)
            returnString += newChar

        return returnString


# Testing


class DefaultTest(unittest.TestCase):
    class TestVingenere:

        # VINGENERE # def vingenere(self,string=None,keyString=None):
        def __init__(self):
            self.aE = DefaultTest().assertEqual
            self.testVingenere()

        def testVingenere(self):
            C = Crypto()
            self.testVingenereExpected(C)
            self.testVingenereUserInput(C)
            self.testVingenereBadInput(C)

        # Vingenere expected input
        def testVingenereExpected(self, C):
            DefaultTest().assertEqual(C.vingenere('?apples?', 'B'), '?bqqmft?')  # OK
            self.aE(C.vingenere('ap_ples', 'A'), 'ap_ples')  # OK
            self.aE(C.vingenere('apple**s', 'AB'), 'aqpme**t')  # OK
            self.aE(C.vingenere('apples!', 'Z'), 'zookdr!')  # OK
            self.aE(C.vingenere('The crow flies at midnight!', 'BooM'),
                    'Uvs osck rmwse bh auebwsih!')  # OK
            # VINGENERE capital
            self.aE(C.vingenere('APP LES', 'b'), 'BQQ MFT')  # OK
            self.aE(C.vingenere(' APPLES ', 'a'), ' APPLES ')  # OK
            self.aE(C.vingenere('APP  LES', 'ab'), 'AQP  MET')  # OK
            self.aE(C.vingenere('APP LES', 'z'), 'ZOO KDR')  # OK
            self.aE(C.vingenere('tHE CROW FILES AT MIDNIGHT?', 'bOOm'),
                    'uVS OSCK RJZSE BH AUEBWSIH?')  # OK

        # User Input Vingenere
        def testVingenereUserInput(self, C):
            print("\nUser Input:\nTesting Vingenere (string:str, keyString:str)\n")
            print(C.vingenere(), end='')  # Asks for user input
            _input = input(
                u"\u0009 <<< Is this correct? (y/n)\n\u0009\u0009NOTE: 'a' rotates 0, not 1\n").lower()
            if _input in ['y', 'yes']:
                print("Passed\n")
            elif _input in ['n', 'no']:
                raise AssertionError(
                    "Test Failed at user input for vingenere(string:str, keyString:str)")
            else:
                print("Skipped\n")

        # BAD INPUT VINGENERE
        def testVingenereBadInput(self, C):
            self.aE(C.vingenere(
                '', 'ok'), "# vingenere(string:str, keyString:str) # <class 'ValueError'> # Enter a valid string")  # OK
            self.aE(C.vingenere(
                0, 'ok'), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.vingenere(
                [], 'ok'), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.vingenere(
                {}, 'ok'), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.vingenere(
                True, 'ok'), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.vingenere(
                'ok', []), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                'ok', {}), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                'ok', 0), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                'ok', ''), "# vingenere(string:str, keyString:str) # <class 'ValueError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                'ok', True), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            print("Vingenere Passed\n")

    class TestCaesar:

        # CAESAR # def caesar(self,string=None,keyNum=None):
        def __init__(self):
            self.aE = DefaultTest().assertEqual
            self.testCaesar()

        def testCaesar(self):
            C = Crypto()
            self.testCaesarExpected(C)
            self.testCaesarUserInput(C)
            self.testCaesarBadInput(C)

        # CAESAR Expected Inputs
        def testCaesarExpected(self, C):
            self.aE(C.caesar('a', 1), 'b')  # OK
            self.aE(C.caesar('apple', 26), 'apple')  # OK
            self.aE(C.caesar('a', -1), 'z')  # OK
            self.aE(C.caesar('a', -27), 'z')  # OK
            self.aE(C.caesar('a', 27), 'b')  # OK
            self.aE(C.caesar('a', 0), 'a')  # OK
            self.aE(C.caesar('b', 1), 'c')  # OK
            self.aE(C.caesar('Hello, World!', 5), 'Mjqqt, Btwqi!')  # OK
            # CAESAR capital
            self.aE(C.caesar('A', 1), 'B')  # OK
            self.aE(C.caesar('APPLE', 26), 'APPLE')  # OK
            self.aE(C.caesar('A', 27), 'B')  # OK
            self.aE(C.caesar('A', -27), 'Z')  # OK
            self.aE(C.caesar('A', -1), 'Z')  # OK
            self.aE(C.caesar('A', 0), 'A')  # OK
            self.aE(C.caesar('B', 1), 'C')  # OK
            self.aE(C.caesar('W', 5), 'B')  # OK

        # CAESAR user input
        def testCaesarUserInput(self, C):
            print("\n\nUser Input:\nTesting caesar(string:str, keyNum:int)\n")
            print(C.caesar(), end='')  # OK # Asks for user input
            _input = input(u"\u0009 <<< Is this correct? (y/n)\n").lower()
            if _input in ['y', 'yes']:
                print("Passed\n")
            elif _input in ['n', 'no']:
                raise AssertionError("Test Failed at user input for caesar(string:str, keyNum:int)")
            else:
                print("Skipped\n")

        # BAD INPUT VINGENERE
        def testCaesarBadInput(self, C):
            self.aE(C.caesar(
                '', 1), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid string")  # OK
            self.aE(C.caesar(
                0, 1), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.caesar(
                True, 1), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.caesar(
                {}, 1), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.caesar(
                [], 1), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid string")  # OK
            self.aE(C.caesar(
                'ok', ''), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar(
                'ok', 'hello'), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar(
                'ok', True), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")   # OK
            self.aE(C.caesar(
                'ok', []), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar(
                'ok', {}), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar(
                'ok', 'oh'), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")  # OK
            print("Caesar Passed\n")

    def testAll(self):
        self.TestVingenere()
        self.TestCaesar()
        print("\nAll Passed", end='')


if __name__ == '__main__':
    unittest.main()
    # Crypto().caesar()
