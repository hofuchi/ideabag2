#! python3
import unittest


class String:
    def __init__(self, string):
        self.string = string

    def countVowels(self):
        string = self.string
        vowellist = ['a', 'e', 'i', 'o', 'u']
        outputnum = 0
        for ite in vowellist:
            while ite in string:
                string = string.replace(ite, '', 1)
                outputnum += 1

        return outputnum


class MyTest(unittest.TestCase):
    def test_me(self):
        s = String('The quick brown fox jumped over the dog')
        self.assertEqual(s.countVowels(), (11))  # OK
        s = String('No')
        self.assertEqual(s.countVowels(), (1))  # OK


unittest.main()
