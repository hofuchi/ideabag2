#! python3
import unittest


class TaxCalculator:
    def __init__(self):
        pass

    def calculateTax(self, total, taxpercent):
        if '%' in taxpercent:
            taxpercent = float(taxpercent.replace('%', '')) / 100

        total = int(total)
        tax = total * taxpercent
        totalplustax = total + tax

        return totalplustax, tax


class MyTest(unittest.TestCase):
    def test_me(self):
        t = TaxCalculator()
        self.assertEqual(t.calculateTax(15, '15%'), (17.25, 2.25))  # OK


unittest.main()
