#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division
import unittest
import sys
from PyQt5.QtWidgets import (QWidget, QLabel, QGridLayout, QApplication, QPushButton, QSizePolicy)
from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtGui import QKeyEvent, QFont
import linecache
# Doesn't do multiple sets of parenthesis


class Calculator:
    def __init__(self):
        pass

    def calculate(self, problem):
        repeat = False
        negativity = 0
        problem = problem.replace('^', '**')
        problemparts = []
        operators = ['(', ')', '**', '*', '/', '+', '-']
        if '..' in problem or problem == '.' or 'E' in problem or problem == '' or 'float division by zero' in problem:
            return 'Error'
        try:
            while ' ' in problem:
                problem = problem.replace(' ', '')
            if '(' in problem:
                # problem = problem.replace('(', '', 1)
                # problem = problem.replace(')', '', 1)
                first = problem.index('(')
                last = problem.index(')')
                problempart = problem[first:last]
                try:
                    int(float(problem[first + 1:last]))
                    parenleftindex = problem.index('(')
                    if int(float(problem[parenleftindex - 1])):
                        problem = problem.replace('(', '*', 1)
                    else:
                        problem = problem.replace('(', '', 1)
                    problem = problem.replace(')', '', 1)
                except Exception:
                    pass
                while '**' in problempart:
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first + 1:last]
                    problempart = problempart.split('**', 1)
                    indexoperator0 = 99
                    indexoperator1 = 99
                    try:
                        float(problempart[1][0])
                        indexoperator1 = 0
                    except Exception:
                        try:
                            float(problempart[1][:2])
                            indexoperator1 = 0
                        except Exception:
                            try:
                                float(problempart[1][1])
                                indexoperator1 = 1
                            except Exception:
                                indexoperator1 = 2
                    try:
                        float(problempart[0][0])
                        indexoperator0 = 0
                    except Exception:
                        try:
                            float(problempart[0][:2])
                            indexoperator0 = 0
                        except Exception:
                            try:
                                float(problempart[0][1])
                                indexoperator0 = 1
                            except Exception:
                                indexoperator0 = 2
                    for num in range(len(problempart[1])):
                        try:
                            pbp1 = problempart[1].replace('.', '')
                            float(pbp1[num])
                            indexoperator11 = num + 1
                        except Exception:
                            if problempart[1][0] in operators:
                                continue
                            else:
                                break
                    for num in range(len(problempart[0])):
                        try:
                            pbp0 = problempart[0].replace('.', '')
                            float(pbp0[num])
                            indexoperator00 = num + 1
                        except Exception:
                            if problempart[0][0] in operators:
                                continue
                            else:
                                break
                    if str(problempart[0][0]) == '-':
                        problempartanswer = float(
                            problempart[0][indexoperator0 + 1:indexoperator00])**float(problempart[1][indexoperator1:indexoperator11])
                    else:
                        problempartanswer = float(
                            problempart[0][indexoperator0:indexoperator00])**float(problempart[1][indexoperator1:indexoperator11])
                    if 'e' in str(problempartanswer):
                        return 'Error: num out of range'
                    indexoperator = 99
                    for ite in operators:
                        if ite in problempart[1]:
                            indexoperator = problempart[1].index(ite)
                    problem = problem.split('(', 1)
                    problem[1] = problem[1].replace(problempart[0][indexoperator0:indexoperator00], '', 1)
                    problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                    problem[1] = problem[1].replace('**', '', 1)
                    if str(problempart[0][0]) == '-':
                        problem[0] += '(-' + str(problempartanswer)
                    else:
                        problem[0] += '(' + str(problempartanswer)
                    problem = ''.join(problem)
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first:last - 1]
                    try:
                        int(float(problem[first + 1:last - 1]))
                        parenleftindex = problem.index('(')
                        try:
                            if int(float(problem[parenleftindex - 1])):
                                problem = problem.replace('(', '*', 1)
                            else:
                                problem = problem.replace('(', '', 1)
                        except Exception:
                            problem = problem.replace('(', '', 1)
                        problem = problem.replace(')', '', 1)
                    except Exception:
                        pass
                while '*' in problempart:
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first + 1:last]
                    problempart = problempart.split('*', 1)
                    indexoperator0 = 99
                    indexoperator1 = 99
                    try:
                        float(problempart[1][0])
                        indexoperator1 = 0
                    except Exception:
                        try:
                            float(problempart[1][:2])
                            indexoperator1 = 0
                        except Exception:
                            try:
                                float(problempart[1][1])
                                indexoperator1 = 1
                            except Exception:
                                indexoperator1 = 2
                    try:
                        float(problempart[0][0])
                        indexoperator0 = 0
                    except Exception:
                        try:
                            float(problempart[0][:2])
                            indexoperator0 = 0
                        except Exception:
                            try:
                                float(problempart[0][1])
                                indexoperator0 = 1
                            except Exception:
                                indexoperator0 = 2
                    for num in range(len(problempart[1])):
                        try:
                            pbp1 = problempart[1].replace('.', '')
                            float(pbp1[num])
                            indexoperator11 = num + 1
                        except Exception:
                            if problempart[1][0] in operators:
                                continue
                            else:
                                break
                    for num in range(len(problempart[0])):
                        try:
                            pbp0 = problempart[0].replace('.', '')
                            float(pbp0[num])
                            indexoperator00 = num + 1
                        except Exception:
                            if problempart[0][0] in operators:
                                continue
                            else:
                                break
                    problempartanswer = float(
                        problempart[0][indexoperator0:indexoperator00]) * float(problempart[1][indexoperator1:indexoperator11])
                    if 'e' in str(problempartanswer):
                        return 'Error: num out of range'
                    indexoperator = 99
                    for ite in operators:
                        if ite in problempart[1]:
                            indexoperator = problempart[1].index(ite)
                    problem = problem.split('(', 1)
                    problem[1] = problem[1].replace(problempart[0][indexoperator0:indexoperator00], '', 1)
                    problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                    problem[1] = problem[1].replace('*', '', 1)
                    problem[0] += '(' + str(problempartanswer)
                    problem = ''.join(problem)
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first:last - 1]
                    try:
                        int(float(problem[first + 1:last - 1]))
                        parenleftindex = problem.index('(')
                        try:
                            if int(float(problem[parenleftindex - 1])):
                                problem = problem.replace('(', '*', 1)
                            else:
                                problem = problem.replace('(', '', 1)
                        except Exception:
                            problem = problem.replace('(', '', 1)
                        problem = problem.replace(')', '', 1)
                    except Exception:
                        pass
                while '/' in problempart:
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first + 1:last]
                    problempart = problempart.split('/', 1)
                    indexoperator0 = 99
                    indexoperator1 = 99
                    try:
                        float(problempart[1][0])
                        indexoperator1 = 0
                    except Exception:
                        try:
                            float(problempart[1][:2])
                            indexoperator1 = 0
                        except Exception:
                            try:
                                float(problempart[1][1])
                                indexoperator1 = 1
                            except Exception:
                                indexoperator1 = 2
                    try:
                        float(problempart[0][0])
                        indexoperator0 = 0
                    except Exception:
                        try:
                            float(problempart[0][:2])
                            indexoperator0 = 0
                        except Exception:
                            try:
                                float(problempart[0][1])
                                indexoperator0 = 1
                            except Exception:
                                indexoperator0 = 2
                    for num in range(len(problempart[1])):
                        try:
                            pbp1 = problempart[1].replace('.', '')
                            float(pbp1[num])
                            indexoperator11 = num + 1
                        except Exception:
                            if problempart[1][0] in operators:
                                continue
                            else:
                                break
                    for num in range(len(problempart[0])):
                        try:
                            pbp0 = problempart[0].replace('.', '')
                            float(pbp0[num])
                            indexoperator00 = num + 1
                        except Exception:
                            if problempart[0][0] in operators:
                                continue
                            else:
                                break
                    problempartanswer = float(
                        problempart[0][indexoperator0:indexoperator00]) / float(problempart[1][indexoperator1:indexoperator11])
                    if 'e' in str(problempartanswer):
                        return 'Error: num out of range'
                    indexoperator = 99
                    for ite in operators:
                        if ite in problempart[1]:
                            indexoperator = problempart[1].index(ite)
                    problem = problem.split('(', 1)
                    problem[1] = problem[1].replace(problempart[0][indexoperator0:indexoperator00], '', 1)
                    problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                    problem[1] = problem[1].replace('/', '', 1)
                    problem[0] += '(' + str(problempartanswer)
                    problem = ''.join(problem)
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first:last - 1]
                    try:
                        int(float(problem[first + 1:last - 1]))
                        parenleftindex = problem.index('(')
                        try:
                            if int(float(problem[parenleftindex - 1])):
                                problem = problem.replace('(', '*', 1)
                            else:
                                problem = problem.replace('(', '', 1)
                        except Exception:
                            problem = problem.replace('(', '', 1)
                        problem = problem.replace(')', '', 1)
                    except Exception:
                        pass
                while '+' in problempart:
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first + 1:last]
                    problempart = problempart.split('+', 1)
                    indexoperator0 = 99
                    indexoperator1 = 99
                    try:
                        float(problempart[1][0])
                        indexoperator1 = 0
                    except Exception:
                        try:
                            float(problempart[1][:2])
                            indexoperator1 = 0
                        except Exception:
                            try:
                                float(problempart[1][1])
                                indexoperator1 = 1
                            except Exception:
                                indexoperator1 = 2
                    try:
                        float(problempart[0][0])
                        indexoperator0 = 0
                    except Exception:
                        try:
                            float(problempart[0][:2])
                            indexoperator0 = 0
                        except Exception:
                            try:
                                float(problempart[0][1])
                                indexoperator0 = 1
                            except Exception:
                                indexoperator0 = 2
                    for num in range(len(problempart[1])):
                        try:
                            pbp1 = problempart[1].replace('.', '')
                            float(pbp1[num])
                            indexoperator11 = num + 1
                        except Exception:
                            if problempart[1][0] in operators:
                                continue
                            else:
                                break
                    for num in range(len(problempart[0])):
                        try:
                            pbp0 = problempart[0].replace('.', '')
                            float(pbp0[num])
                            indexoperator00 = num + 1
                        except Exception:
                            if problempart[0][0] in operators:
                                continue
                            else:
                                break
                    problempartanswer = float(
                        problempart[0][indexoperator0:indexoperator00]) + float(problempart[1][indexoperator1:indexoperator11])
                    if 'e' in str(problempartanswer):
                        return 'Error: num out of range'
                    indexoperator = 99
                    for ite in operators:
                        if ite in problempart[1]:
                            indexoperator = problempart[1].index(ite)
                    problem = problem.split('(', 1)
                    problem[1] = problem[1].replace(problempart[0][indexoperator0:indexoperator00], '', 1)
                    problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                    problem[1] = problem[1].replace('+', '', 1)
                    problem[0] += '(' + str(problempartanswer)
                    problem = ''.join(problem)
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first:last - 1]
                    try:
                        int(float(problem[first + 1:last - 1]))
                        parenleftindex = problem.index('(')
                        try:
                            if int(float(problem[parenleftindex - 1])):
                                problem = problem.replace('(', '*', 1)
                            else:
                                problem = problem.replace('(', '', 1)
                        except Exception:
                            problem = problem.replace('(', '', 1)
                        problem = problem.replace(')', '', 1)
                    except Exception:
                        pass
                while '-' in problempart:
                    if problempart[0] == '-':
                        break
                    if problem[0] == '-':
                        break
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first + 1:last]
                    problempart = problempart.split('-', 1)
                    indexoperator0 = 99
                    indexoperator1 = 99
                    try:
                        float(problempart[1][0])
                        indexoperator1 = 0
                    except Exception:
                        try:
                            float(problempart[1][:2])
                            indexoperator1 = 0
                        except Exception:
                            try:
                                float(problempart[1][1])
                                indexoperator1 = 1
                            except Exception:
                                indexoperator1 = 2
                    try:
                        float(problempart[0][0])
                        indexoperator0 = 0
                    except Exception:
                        try:
                            float(problempart[0][:2])
                            indexoperator0 = 0
                        except Exception:
                            try:
                                float(problempart[0][1])
                                indexoperator0 = 1
                            except Exception:
                                indexoperator0 = 2
                    for num in range(len(problempart[1])):
                        try:
                            pbp1 = problempart[1].replace('.', '')
                            float(pbp1[num])
                            indexoperator11 = num + 1
                        except Exception:
                            if problempart[1][0] in operators:
                                continue
                            else:
                                break
                    for num in range(len(problempart[0])):
                        try:
                            pbp0 = problempart[0].replace('.', '')
                            float(pbp0[num])
                            indexoperator00 = num + 1
                        except Exception:
                            if problempart[0][0] in operators:
                                continue
                            else:
                                break
                    problempartanswer = float(
                        problempart[0][indexoperator0:indexoperator00]) - float(problempart[1][indexoperator1:indexoperator11])
                    if 'e' in str(problempartanswer):
                        return 'Error: num out of range'
                    indexoperator = 99
                    for ite in operators:
                        if ite in problempart[1]:
                            indexoperator = problempart[1].index(ite)
                    problem = problem.split('(', 1)
                    problem[1] = problem[1].replace(problempart[0][indexoperator0:indexoperator00], '', 1)
                    problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                    problem[1] = problem[1].replace('-', '', 1)
                    problem[0] += '(' + str(problempartanswer)
                    problem = ''.join(problem)
                    first = problem.index('(')
                    last = problem.index(')')
                    problempart = problem[first:last - 1]
                    try:
                        int(float(problem[first + 1:last - 1]))
                        parenleftindex = problem.index('(')
                        try:
                            if int(float(problem[parenleftindex - 1])):
                                problem = problem.replace('(', '*', 1)
                            else:
                                problem = problem.replace('(', '', 1)
                        except Exception:
                            problem = problem.replace('(', '', 1)
                        problem = problem.replace(')', '', 1)
                    except Exception:
                        pass
            while '**' in problem:
                first = 0
                problempart = problem[first:]
                problempart = problempart.split('**', 1)
                indexoperator0 = 99
                indexoperator1 = 99
                indexoperator11 = None
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator1 = num
                        break
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                indexoperator1 = num
                                break
                            else:
                                continue
                        else:
                            break
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator11 = num + 1
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                if indexoperator11 is not None:
                                    break
                                else:
                                    continue
                            else:
                                break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator0 = len(pbp0) - num - 1
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            if problempart[0][len(pbp0) - num - 1] == '-':
                                try:
                                    if len(pbp0) - num - 2 < 0:
                                        raise
                                    int(problempart[0][len(pbp0) - num - 2])
                                except Exception:
                                    indexoperator0 -= 1
                                '''
                                try:
                                    int(problempart[0][len(pbp0)-num-2])
                                    indexoperator0 = len(pbp0)-num
                                    break
                                except Exception:
                                    continue
                                '''
                            break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator00 = len(pbp0) + num
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            continue
                        else:
                            break
                problempartanswer = float(
                    problempart[0][indexoperator0:indexoperator00])**float(problempart[1][indexoperator1:indexoperator11])
                if 'e' in str(problempartanswer):
                    return 'Error: num out of range'
                indexoperator = 99
                for ite in operators:
                    if ite in problempart[1]:
                        indexoperator = problempart[1].index(ite)
                problem = problem.split('**', 1)
                k = problem[0].rfind(problempart[0][indexoperator0:indexoperator00])
                problem[0] = problem[0][:k] + '' + problem[0][k + len(problem[0]):]
                # problem[0] = problem[0].replace(problempart[0][indexoperator0:indexoperator00], '', -1)
                problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                problem[0] += str(problempartanswer)
                problem = ''.join(problem)
                # problem = problem.replace('*', '', 1)
                first = 0
                try:
                    int(float(problem[first + 1:]))
                except Exception:
                    pass
                problempart = problem[first:]
            while '*' in problem:
                if '/' in problem:
                    if problem.index('/') < problem.index('*'):
                        repeat = True
                        break
                first = 0
                problempart = problem[first:]
                problempart = problempart.split('*', 1)
                indexoperator0 = 99
                indexoperator1 = 99
                indexoperator11 = None
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator1 = num
                        break
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                indexoperator1 = num
                                break
                            else:
                                continue
                        else:
                            break
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator11 = num + 1
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                if indexoperator11 is not None:
                                    break
                                else:
                                    continue
                            else:
                                break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator0 = len(pbp0) - num - 1
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            if problempart[0][len(pbp0) - num - 1] == '-':
                                try:
                                    if len(pbp0) - num - 2 < 0:
                                        raise
                                    int(problempart[0][len(pbp0) - num - 2])
                                except Exception:
                                    indexoperator0 -= 1
                            break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator00 = len(pbp0) + num
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            continue
                        else:
                            break
                problempartanswer = float(problempart[0][indexoperator0:indexoperator00]) * \
                    float(problempart[1][indexoperator1:indexoperator11])
                if 'e' in str(problempartanswer):
                    return 'Error: num out of range'
                indexoperator = 99
                for ite in operators:
                    if ite in problempart[1]:
                        indexoperator = problempart[1].index(ite)
                problem = problem.split('*', 1)
                k = problem[0].rfind(problempart[0][indexoperator0:indexoperator00])
                problem[0] = problem[0][:k] + '' + problem[0][k + len(problem[0]):]
                # problem[0] = problem[0].replace(problempart[0][indexoperator0:indexoperator00], '', -1)
                problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                problem[0] += str(problempartanswer)
                problem = ''.join(problem)
                # problem = problem.replace('*', '', 1)
                first = 0
                try:
                    int(float(problem[first + 1:]))
                except Exception:
                    pass
                problempart = problem[first:]
                # print (problem)
            while '/' in problem:
                first = 0
                problempart = problem[first:]
                problempart = problempart.split('/', 1)
                indexoperator0 = 99
                indexoperator1 = 99
                indexoperator11 = None
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator1 = num
                        break
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                indexoperator1 = num
                                break
                            else:
                                continue
                        else:
                            break
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator11 = num + 1
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                if indexoperator11 is not None:
                                    break
                                else:
                                    continue
                            else:
                                break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator0 = len(pbp0) - num - 1
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            if problempart[0][len(pbp0) - num - 1] == '-':
                                try:
                                    if len(pbp0) - num - 2 < 0:
                                        raise
                                    int(problempart[0][len(pbp0) - num - 2])
                                except Exception:
                                    indexoperator0 -= 1
                            break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator00 = len(pbp0) + num
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            continue
                        else:
                            break
                problempartanswer = float(problempart[0][indexoperator0:indexoperator00]) / \
                    float(problempart[1][indexoperator1:indexoperator11])
                if 'e' in str(problempartanswer):
                    return 'Error: num out of range'
                indexoperator = 99
                for ite in operators:
                    if ite in problempart[1]:
                        indexoperator = problempart[1].index(ite)
                problem = problem.split('/', 1)
                k = problem[0].rfind(problempart[0][indexoperator0:indexoperator00])
                problem[0] = problem[0][:k] + '' + problem[0][k + len(problem[0]):]
                # problem[0] = problem[0].replace(problempart[0][indexoperator0:indexoperator00], '', -1)
                problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                problem[0] += str(problempartanswer)
                problem = ''.join(problem)
                # problem = problem.replace('*', '', 1)
                first = 0
                try:
                    int(float(problem[first + 1:]))
                except Exception:
                    pass
                problempart = problem[first:]
                if repeat is True:
                    problem = self.calculate(str(problem))
                    try:
                        int(float(problem))
                        return problem
                    except Exception:
                        pass
            if '--' in problem:
                problem = problem.replace('--', '+')
            while '+' in problem:
                triggered = 0
                first = 0
                problempart = problem[first:]
                problempart = problempart.split('+', 1)
                indexoperator0 = 99
                indexoperator1 = 99
                indexoperator11 = None
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator1 = num
                        break
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                indexoperator1 = num
                                break
                            else:
                                continue
                        else:
                            break
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator11 = num + 1
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                if indexoperator11 is not None:
                                    break
                                else:
                                    continue
                            else:
                                break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator0 = len(pbp0) - num - 1
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            if problempart[0][len(pbp0) - num - 1] == '-':
                                try:
                                    if len(pbp0) - num - 2 < 0:
                                        raise
                                    int(problempart[0][len(pbp0) - num - 2])
                                    triggered = 1
                                    indexoperator0 -= 1
                                except Exception:
                                    indexoperator0 -= 1
                            break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator00 = len(pbp0) + num
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            continue
                        else:
                            break
                problempartanswer = float(problempart[0][indexoperator0:indexoperator00]) + \
                    float(problempart[1][indexoperator1:indexoperator11])
                if 'e' in str(problempartanswer):
                    return 'Error: num out of range'
                indexoperator = 99
                for ite in operators:
                    if ite in problempart[1]:
                        indexoperator = problempart[1].index(ite)
                problem = problem.split('+', 1)
                k = problem[0].rfind(problempart[0][indexoperator0:indexoperator00])
                problem[0] = problem[0][:k] + '' + problem[0][k + len(problem[0]):]
                # problem[0] = problem[0].replace(problempart[0][indexoperator0:indexoperator00], '', -1)
                problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                if triggered == 1:
                    problem[0] += '+' + str(problempartanswer)
                else:
                    problem[0] += str(problempartanswer)
                problem = ''.join(problem)
                # problem = problem.replace('*', '', 1)
                first = 0
                try:
                    int(float(problem[first + 1:]))
                except Exception:
                    pass
                problempart = problem[first:]
            while '-' in problem:
                if problem[0] == '-' and '-' not in problem[1:]:
                    break
                first = 0
                problempart = problem[first:]
                if problempart[0] == '-':
                    problempart = problempart[1:].split('-', 1)
                    problempart[0] = '-' + problempart[0]
                else:
                    problempart = problempart.split('-', 1)
                indexoperator0 = 99
                indexoperator1 = 99
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator1 = num
                        break
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                indexoperator1 = num
                                break
                            else:
                                continue
                        else:
                            break
                for num in range(len(problempart[1])):
                    try:
                        pbp1 = problempart[1].replace('.', '0')
                        float(pbp1[num])
                        indexoperator11 = num + 1
                    except Exception:
                        if problempart[1][num] in operators:
                            if problempart[1][num] == '-':
                                break
                            else:
                                break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator0 = len(pbp0) - num - 1
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            if problempart[0][len(pbp0) - num - 1] == '-':
                                try:
                                    if len(pbp0) - num - 2 < 0:
                                        raise
                                    int(problempart[0][len(pbp0) - num - 2])
                                except Exception:
                                    indexoperator0 -= 1
                                '''
                                try:
                                    int(problempart[0][len(pbp0)-num-2])
                                    indexoperator0 = len(pbp0)-num
                                    break
                                except Exception:
                                    continue
                                '''
                            break
                        else:
                            continue
                for num in range(len(problempart[0])):
                    try:
                        pbp0 = problempart[0].replace('.', '0')
                        float(pbp0[len(pbp0) - num - 1])
                        indexoperator00 = len(pbp0) + num
                    except Exception:
                        if problempart[0][len(pbp0) - num - 1] in operators:
                            continue
                        else:
                            break
                problempartanswer = float(problempart[0][indexoperator0:indexoperator00]) - \
                    float(problempart[1][indexoperator1:indexoperator11])
                if 'e' in str(problempartanswer):
                    return 'Error: num out of range'
                indexoperator = 99
                for ite in operators:
                    if ite in problempart[1]:
                        indexoperator = problempart[1].index(ite)
                if problem[0] == '-':
                    problem = problem[1:].split('-', 1)
                    problem[0] = '-' + problem[0]
                else:
                    problem = problem.split('-', 1)
                k = problem[0].rfind(problempart[0][indexoperator0:indexoperator00])
                problem[0] = problem[0][:k] + '' + problem[0][k + len(problem[0]):]
                # problem[0] = problem[0].replace(problempart[0][indexoperator0:indexoperator00], '', -1)
                problem[1] = problem[1].replace(problempart[1][indexoperator1:indexoperator11], '', 1)
                problem[0] += str(problempartanswer)
                problem = ''.join(problem)
                # problem = problem.replace('*', '', 1)
                first = 0
                try:
                    int(float(problem[first + 1:]))
                except Exception:
                    pass
                problempart = problem[first:]
        except Exception as e:
            print(problem)
            print(e)
            if str(e) == 'float division by zero':
                return e
            exc_type, exc_obj, tb = sys.exc_info()
            f = tb.tb_frame
            lineno = tb.tb_lineno
            filename = f.f_code.co_filename
            linecache.checkcache(filename)
            line = linecache.getline(filename, lineno, f.f_globals)
            print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))
            return 'Error'

        if negativity == 1:
            problem = '-' + problem
        if problem[-2:] == '.0':
            return int(problem[:-2])
        elif str(float(problem))[-2:] == '.0':
            return int(problem)
        return float(problem)


'''
class MyTest(unittest.TestCase):
    def test_me(self):
        c = Calculator()
        self.assertEqual(c.calculate('(27 ** 3 * 2 / 2 + 2 - 2) / 2'), (9841.5)) #OK
        self.assertEqual(c.calculate('13 / 2'), (6.5)) #OK
        self.assertEqual(c.calculate('3 + 13 / 2'), (9.5)) #OK
        self.assertEqual(c.calculate('33 - 13 / 2'), (26.5)) #OK
        self.assertEqual(c.calculate('14 / 2'), (7.0)) #OK
        self.assertEqual(c.calculate('33 - 13 + 2 / 2.0 + 2'), (17.0)) #OK

unittest.main()
'''


class CalculatorGUI(QWidget):
    def __init__(self):
        super().__init__()
        self.clear = 0
        self.initUI()
        self.buttonFontSize = 14

    def initUI(self):
        self.initialHeight = 400
        self.previousAnswer = QLabel()
        self.answer = QLabel()
        self.font = QFont()
        self.font.setPointSize(16)
        self.previousAnswer.setFont(self.font)
        self.font.setPixelSize(40)
        self.answer.setFont(self.font)
        self.buttonFont = QFont()
        self.buttonFont.setPointSize(14)

        self.equalButton = QPushButton("=")
        self.clearButton = QPushButton("Clear")
        self.oneButton = QPushButton("1")
        self.twoButton = QPushButton("2")
        self.threeButton = QPushButton("3")
        self.fourButton = QPushButton("4")
        self.fiveButton = QPushButton("5")
        self.sixButton = QPushButton("6")
        self.sevenButton = QPushButton("7")
        self.eightButton = QPushButton("8")
        self.nineButton = QPushButton("9")
        self.zeroButton = QPushButton("0")
        self.multiplyButton = QPushButton("*")
        self.divideButton = QPushButton("/")
        self.subtractButton = QPushButton("-")
        self.addButton = QPushButton("+")
        self.exponentButton = QPushButton("^")
        self.dotButton = QPushButton(".")
        self.backSpaceButton = QPushButton("Backspace")
        self.parenleftButton = QPushButton("(")
        self.parenrightButton = QPushButton(")")
        self.ansButton = QPushButton("Ans")

        self.equalButton.setFont(self.buttonFont)
        self.clearButton.setFont(self.buttonFont)
        self.oneButton.setFont(self.buttonFont)
        self.twoButton.setFont(self.buttonFont)
        self.threeButton.setFont(self.buttonFont)
        self.fourButton.setFont(self.buttonFont)
        self.fiveButton.setFont(self.buttonFont)
        self.sixButton.setFont(self.buttonFont)
        self.sevenButton.setFont(self.buttonFont)
        self.eightButton.setFont(self.buttonFont)
        self.nineButton.setFont(self.buttonFont)
        self.zeroButton.setFont(self.buttonFont)
        self.multiplyButton.setFont(self.buttonFont)
        self.divideButton.setFont(self.buttonFont)
        self.subtractButton.setFont(self.buttonFont)
        self.addButton.setFont(self.buttonFont)
        self.exponentButton.setFont(self.buttonFont)
        self.dotButton.setFont(self.buttonFont)
        self.backSpaceButton.setFont(self.buttonFont)
        self.parenleftButton.setFont(self.buttonFont)
        self.parenrightButton.setFont(self.buttonFont)
        self.ansButton.setFont(self.buttonFont)

        self.parenrightButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.parenleftButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.ansButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.equalButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.clearButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.oneButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.twoButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.threeButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.fourButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.fiveButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.sixButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.sevenButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.eightButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.nineButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.zeroButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.multiplyButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.divideButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.subtractButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.addButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.exponentButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.dotButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.backSpaceButton.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)

        self.ansButton.clicked.connect(self.buttonClicked)
        self.parenrightButton.clicked.connect(self.buttonClicked)
        self.parenleftButton.clicked.connect(self.buttonClicked)
        self.equalButton.clicked.connect(self.buttonClicked)
        self.clearButton.clicked.connect(self.buttonClicked)
        self.oneButton.clicked.connect(self.buttonClicked)
        self.twoButton.clicked.connect(self.buttonClicked)
        self.threeButton.clicked.connect(self.buttonClicked)
        self.fourButton.clicked.connect(self.buttonClicked)
        self.fiveButton.clicked.connect(self.buttonClicked)
        self.sixButton.clicked.connect(self.buttonClicked)
        self.sevenButton.clicked.connect(self.buttonClicked)
        self.eightButton.clicked.connect(self.buttonClicked)
        self.nineButton.clicked.connect(self.buttonClicked)
        self.zeroButton.clicked.connect(self.buttonClicked)
        self.multiplyButton.clicked.connect(self.buttonClicked)
        self.divideButton.clicked.connect(self.buttonClicked)
        self.subtractButton.clicked.connect(self.buttonClicked)
        self.addButton.clicked.connect(self.buttonClicked)
        self.exponentButton.clicked.connect(self.buttonClicked)
        self.dotButton.clicked.connect(self.buttonClicked)
        self.backSpaceButton.clicked.connect(self.buttonClicked)

        grid = QGridLayout()
        grid.setSpacing(5)
        grid.addWidget(self.clearButton, 2, 0)
        grid.addWidget(self.backSpaceButton, 2, 1, 1, 2)
        grid.addWidget(self.exponentButton, 2, 3)
        grid.addWidget(self.sevenButton, 3, 0)
        grid.addWidget(self.eightButton, 3, 1)
        grid.addWidget(self.nineButton, 3, 2)
        grid.addWidget(self.multiplyButton, 3, 3)
        grid.addWidget(self.fourButton, 4, 0)
        grid.addWidget(self.fiveButton, 4, 1)
        grid.addWidget(self.sixButton, 4, 2)
        grid.addWidget(self.divideButton, 4, 3)
        grid.addWidget(self.oneButton, 5, 0)
        grid.addWidget(self.twoButton, 5, 1)
        grid.addWidget(self.threeButton, 5, 2)
        grid.addWidget(self.subtractButton, 5, 3)
        grid.addWidget(self.equalButton, 6, 0, 2, 1)
        grid.addWidget(self.zeroButton, 6, 1)
        grid.addWidget(self.dotButton, 6, 2)
        grid.addWidget(self.addButton, 6, 3)
        grid.addWidget(self.answer, 1, 0, 1, 4)
        grid.addWidget(self.previousAnswer, -0, 0, 1, 4)
        grid.addWidget(self.ansButton, 7, 1)
        grid.addWidget(self.parenleftButton, 7, 2)
        grid.addWidget(self.parenrightButton, 7, 3)

        self.setLayout(grid)
        self.setGeometry(300, 300, 410, 400)
        self.setWindowTitle('Calculator')
        self.show()

    def resizeEvent(self, event):
        self.answer.setFixedHeight((self.height()) * .1)
        self.font.setPixelSize((self.height()) * .1)
        self.answer.setFont(self.font)
        if self.height() > self.initialHeight + 80:
            self.initialHeight += 80
            self.buttonFontSize += 2
            self.buttonFont.setPointSize(self.buttonFontSize)
        if self.height() < self.initialHeight - 80:
            self.initialHeight -= 80
            self.buttonFontSize -= 2
            self.buttonFont.setPointSize(self.buttonFontSize)
        self.equalButton.setFont(self.buttonFont)
        self.clearButton.setFont(self.buttonFont)
        self.oneButton.setFont(self.buttonFont)
        self.twoButton.setFont(self.buttonFont)
        self.threeButton.setFont(self.buttonFont)
        self.fourButton.setFont(self.buttonFont)
        self.fiveButton.setFont(self.buttonFont)
        self.sixButton.setFont(self.buttonFont)
        self.sevenButton.setFont(self.buttonFont)
        self.eightButton.setFont(self.buttonFont)
        self.nineButton.setFont(self.buttonFont)
        self.zeroButton.setFont(self.buttonFont)
        self.multiplyButton.setFont(self.buttonFont)
        self.divideButton.setFont(self.buttonFont)
        self.subtractButton.setFont(self.buttonFont)
        self.addButton.setFont(self.buttonFont)
        self.exponentButton.setFont(self.buttonFont)
        self.dotButton.setFont(self.buttonFont)
        self.backSpaceButton.setFont(self.buttonFont)
        self.parenleftButton.setFont(self.buttonFont)
        self.parenrightButton.setFont(self.buttonFont)
        self.ansButton.setFont(self.buttonFont)

    def keyPressEvent(self, e):
        # print (e.key())
        if e.key() == 94:
            self.exponentButton.click()
        elif e.key() == Qt.Key_Escape:
            self.clearButton.click()
        elif e.key() == 40:
            self.parenleftButton.click()
        elif e.key() == 41:
            self.parenrightButton.click()
        elif e.key() == Qt.Key_Return or e.key() == Qt.Key_Enter:
            self.equalButton.click()
        elif e.key() == Qt.Key_0:
            self.zeroButton.click()
        elif e.key() == Qt.Key_1:
            self.oneButton.click()
        elif e.key() == Qt.Key_2:
            self.twoButton.click()
        elif e.key() == Qt.Key_3:
            self.threeButton.click()
        elif e.key() == Qt.Key_4:
            self.fourButton.click()
        elif e.key() == Qt.Key_5:
            self.fiveButton.click()
        elif e.key() == Qt.Key_6:
            self.sixButton.click()
        elif e.key() == Qt.Key_7:
            self.sevenButton.click()
        elif e.key() == Qt.Key_8:
            self.eightButton.click()
        elif e.key() == Qt.Key_9:
            self.nineButton.click()
        elif e.key() == Qt.Key_Backspace:
            self.backSpaceButton.click()
        elif e.key() == Qt.Key_Minus:
            self.subtractButton.click()
        elif e.key() == Qt.Key_Plus:
            self.addButton.click()
        elif e.key() == Qt.Key_Period:
            self.dotButton.click()
        elif e.key() == Qt.Key_Asterisk:
            self.multiplyButton.click()
        elif e.key() == Qt.Key_Slash:
            self.divideButton.click()
        else:
            pass

    def buttonClicked(self):
        sender = self.sender()
        if 'float' in self.answer.text():
            self.answer.setText('Error')
        if sender.text() == '=':
            self.previousAnswer.setText(self.answer.text() + '=')
            x = Calculator().calculate(self.answer.text())
            self.answer.setText(str(x))
            self.clear = 1
            self.previousAnswer.setText(self.previousAnswer.text() + self.answer.text())
        elif sender.text() == '^':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '(':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == ')':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == 'Ans':
            try:
                equalIndex = self.previousAnswer.text().index('=')
                self.answer.setText(self.answer.text() + self.previousAnswer.text()[equalIndex + 1:])
                self.clear = 0
            except Exception:
                self.answer.setText('Error')
        elif sender.text() == '*':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '/':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '+':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '-':
            if 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
                self.clear = 0
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '0':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '1':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '2':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '3':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '4':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '5':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '6':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '7':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '8':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == '9':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())
        elif sender.text() == 'Clear':
            self.answer.setText(self.answer.clear())
        elif sender.text() == 'Backspace':
            self.answer.setText(self.answer.text()[:-1])
        elif sender.text() == '.':
            if self.clear == 0 and 'Error' not in self.answer.text():
                self.answer.setText(self.answer.text() + sender.text())
            else:
                self.clear = 0
                self.answer.setText(self.answer.clear())
                self.answer.setText(self.answer.text() + sender.text())

    def closeEvent(self, event):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = CalculatorGUI()
    sys.exit(app.exec_())
