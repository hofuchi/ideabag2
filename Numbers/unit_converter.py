#! python3
from __future__ import division
import unittest


class UnitConvert:
    def __init__(self):
        # Temp
        self.flist = ['fahrenheit', 'f']
        self.clist = ['celciu', 'centigrade', 'c']
        self.kelvinlist = ['kelvin', 'k']  # Centre

        # Weight
        self.gramlist = ['gram', 'g']
        self.poundlist = ['pound', 'lb']
        self.metrictonlist = ['metric ton']
        self.ouncelist = ['ounce', 'oz']
        self.kilogramlist = ['kilo', 'kilogram']  # Centre

        # Liquid
        self.pintlist = ['pint', 'pt']
        self.quartlist = ['quart', 'qt']
        self.gallonlist = ['gallon', 'gl']
        self.cuplist = ['cup']
        self.literlist = ['liter', 'l']  # Centre

        # Length
        self.incheslist = ['inch', 'in', 'inche']
        self.feetlist = ['feet', 'ft']
        self.yardlist = ['yard', 'yd']
        self.milelist = ['mile', 'mi']
        self.cmlist = ['centimetre', 'centimeter', 'cm']
        self.mmlist = ['milimetre', 'milimeter', 'mm']
        self.mlist = ['metre', 'meter', 'm']  # Centre
        self.kmlist = ['kilometre', 'kilometer', 'km']

        # Combined Measure Lists
        self.lengthlist = ['inch', 'in', 'inche', 'feet', 'ft', 'yard', 'yd', 'mile', 'mi', 'centimetre',
                           'centimeter', 'cm', 'milimetre', 'milimeter', 'mm', 'metre', 'meter', 'm', 'kilometre', 'kilometer', 'km']
        self.liquidlist = ['pint', 'pt', 'quart', 'qt', 'gallon', 'gl', 'cup', 'liter', 'l']
        self.weightlist = ['gram', 'g', 'pound', 'lb', 'metric ton', 'ounce', 'oz', 'kilo', 'kilogram']
        self.templist = ['fahrenheit', 'f', 'celciu', 'centigrade', 'c', 'kelvin', 'k']

        # Conversions
        self.conversions = {
            'self.mlist': {'self.incheslist': 0.0254, 'self.feetlist': 0.3048, 'self.yardlist': 0.9144, 'self.milelist': 1609.35, 'self.cmlist': 0.01, 'self.mmlist': 0.001, 'self.kmlist': 1000},
            'self.literlist': {'self.cuplist': 0.236588125, 'self.pintlist': 0.47317625, 'self.quartlist': 0.9463525, 'self.gallonlist': 3.78541},
            'self.kilogramlist': {'self.gramlist': 0.001, 'self.poundlist': 0.453592, 'self.metrictonlist': 1000, 'self.ouncelist': 0.0283495}
        }

    def convertUnit(self, value, unitfrom, unitto):
        unitfromlowerstripped = unitfrom.lower().rstrip('s')
        unittolowerstripped = unitto.lower().rstrip('s')

        # Convert to centre unit
        if unitfromlowerstripped in self.lengthlist:
            if unitfromlowerstripped in self.mmlist:
                convert = self.conversions['self.mlist']['self.mmlist'] * value
            elif unitfromlowerstripped in self.kmlist:
                convert = self.conversions['self.mlist']['self.kmlist'] * value
            elif unitfromlowerstripped in self.cmlist:
                convert = self.conversions['self.mlist']['self.cmlist'] * value
            elif unitfromlowerstripped in self.milelist:
                convert = self.conversions['self.mlist']['self.milelist'] * value
            elif unitfromlowerstripped in self.yardlist:
                convert = self.conversions['self.mlist']['self.yardlist'] * value
            elif unitfromlowerstripped in self.feetlist:
                convert = self.conversions['self.mlist']['self.feetlist'] * value
            elif unitfromlowerstripped in self.incheslist:
                convert = self.conversions['self.mlist']['self.incheslist'] * value
        elif unitfromlowerstripped in self.liquidlist:
            if unitfromlowerstripped in self.cuplist:
                convert = self.conversions['self.literlist']['self.cuplist'] * value
            elif unitfromlowerstripped in self.gallonlist:
                convert = self.conversions['self.literlist']['self.gallonlist'] * value
            elif unitfromlowerstripped in self.quartlist:
                convert = self.conversions['self.literlist']['self.quartlist'] * value
            elif unitfromlowerstripped in self.pintlist:
                convert = self.conversions['self.literlist']['self.pintlist'] * value
        elif unitfromlowerstripped in self.weightlist:
            if unitfromlowerstripped in self.poundlist:
                convert = self.conversions['self.kilogramlist']['self.poundlist'] * value
            elif unitfromlowerstripped in self.ouncelist:
                convert = self.conversions['self.kilogramlist']['self.ouncelist'] * value
            elif unitfromlowerstripped in self.metrictonlist:
                convert = self.conversions['self.kilogramlist']['self.metrictonlist'] * value
            elif unitfromlowerstripped in self.gramlist:
                convert = self.conversions['self.kilogramlist']['self.gramlist'] * value
            else:
                pass
        elif unitfromlowerstripped in self.templist:
            if unitfromlowerstripped in self.clist:
                convert = value + 273.15
            elif unitfromlowerstripped in self.flist:
                convert = (value - 32) * 5 / 9 + 273.15
        else:
            return "Unit not available!"

        # Convert from centre unit
        if unittolowerstripped in self.lengthlist:
            if unittolowerstripped in self.mmlist:
                output = convert / self.conversions['self.mlist']['self.mmlist']
            elif unittolowerstripped in self.kmlist:
                output = convert / self.conversions['self.mlist']['self.kmlist']
            elif unittolowerstripped in self.feetlist:
                output = convert / self.conversions['self.mlist']['self.feetlist']
            elif unittolowerstripped in self.yardlist:
                output = convert / self.conversions['self.mlist']['self.yardlist']
            elif unittolowerstripped in self.milelist:
                output = convert / self.conversions['self.mlist']['self.milelist']
            elif unittolowerstripped in self.cmlist:
                output = convert / self.conversions['self.mlist']['self.cmlist']
            elif unittolowerstripped in self.incheslist:
                output = convert / self.conversions['self.mlist']['self.incheslist']
            elif unittolowerstripped in self.mlist:
                if convert is None:
                    output = value
                else:
                    output = convert
        elif unittolowerstripped in self.liquidlist:
            if unittolowerstripped in self.cuplist:
                output = convert / self.conversions['self.literlist']['self.cuplist']
            elif unittolowerstripped in self.gallonlist:
                output = convert / self.conversions['self.literlist']['self.gallonlist']
            elif unittolowerstripped in self.pintlist:
                output = convert / self.conversions['self.literlist']['self.pintlist']
            elif unittolowerstripped in self.quartlist:
                output = convert / self.conversions['self.literlist']['self.quartlist']
            elif unittolowerstripped in self.literlist:
                if convert is None:
                    output = value
                else:
                    output = convert
        elif unittolowerstripped in self.weightlist:
            if unittolowerstripped in self.metrictonlist:
                output = convert / self.conversions['self.kilogramlist']['self.metrictonlist']
            elif unittolowerstripped in self.ouncelist:
                output = convert / self.conversions['self.kilogramlist']['self.ouncelist']
            elif unittolowerstripped in self.poundlist:
                output = convert / self.conversions['self.kilogramlist']['self.poundlist']
            elif unittolowerstripped in self.gramlist:
                output = convert / self.conversions['self.kilogramlist']['self.gramlist']
            elif unittolowerstripped in self.kilogramlist:
                if convert is None:
                    output = value
                else:
                    output = convert
        elif unittolowerstripped in self.templist:
            if unittolowerstripped in self.clist:
                output = convert - 273.15
            elif unittolowerstripped in self.flist:
                output = 9 / 5 * (convert - 273.15) + 32
            elif unittolowerstripped in self.kelvinlist:
                if convert is None:
                    output = value
                else:
                    output = convert
            else:
                pass
        else:
            return "Unit not available!"

        return output, unitto


class MyTest(unittest.TestCase):
    def test_me(self):
        u = UnitConvert()
        self.assertEqual(u.convertUnit(155, 'mm', 'km'), (0.000155, 'km'))  # OK
        self.assertEqual(u.convertUnit(101, 'celcius', 'F'), (213.8, 'F'))  # OK
        self.assertEqual(u.convertUnit(155, 'ounces', 'pounds'), (9.6875, 'pounds'))  # OK
        self.assertEqual(u.convertUnit(101, 'cups', 'liters'), (23.895400625, 'liters'))  # OK


unittest.main()
