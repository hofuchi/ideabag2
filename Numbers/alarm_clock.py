#! python3
import unittest
import time
import winsound
import pytz
from datetime import datetime, date, timedelta


class AlarmClock:
    def __init__(self):
        pass

    def notifyAtACertainTime(self, mydate):
        ts = time.time()
        timezone = 'US/Central'
        central = pytz.timezone(timezone)
        utc_now, now = datetime.utcfromtimestamp(ts), datetime.fromtimestamp(ts)
        # local_now = now.replace(tzinfo=pytz.utc).astimezone(central) # utc -> local
        # assert local_now.replace(tzinfo=None) == now
        local_now = now
        today = local_now
        todaypretty = str(today)[:-13]
        todayprettier = today.date()

        if mydate[:3].lower() == 'tom':
            if mydate[:8].lower() == 'tomorrow':
                mydate1 = mydate[8:]
                mydate = "{:%m %d %Y}".format(today)
                mydate = str(mydate).split(' ')
                day = mydate[1]
                day = int(day)
                day += 1
                day = str(day)
                mydate[1] = day
                mydate = ' '.join(mydate)
                mydate += mydate1
            else:
                mydate1 = mydate[3:]
                mydate = "{:%m %d %Y}".format(today)
                mydate = str(mydate).split(' ')
                day = mydate[1]
                day = int(day)
                day += 1
                day = str(day)
                mydate[1] = day
                mydate = ' '.join(mydate)
                mydate += mydate1

        elif mydate[:3].lower() == 'tod':
            if mydate[:5].lower() == 'today':
                mydate1 = mydate[5:]
                mydate = "{:%m %d %Y}".format(today)
                mydate = str(mydate)
                mydate += mydate1
            else:
                mydate1 = mydate[3:]
                mydate = "{:%m %d %Y}".format(today)
                mydate = str(mydate)
                mydate += mydate1

        if '/' in mydate:
            mydate = mydate.split('/')
            mydate = ' '.join(mydate)
        if '-' in mydate:
            mydate = mydate.split('-')
            mydate = ' '.join(mydate)
        if ',' in mydate:
            mydate = mydate.split(',')
            mydate = ''.join(mydate)
        if ':' in mydate:
            mydate = mydate.split(':')
            mydate = ' '.join(mydate)
        if '.' in mydate:
            mydate = mydate.split('.')
            mydate = ' '.join(mydate)

        formats = ['%b %d %Y %H %M', '%B %d %Y %H %M', '%m %d %Y %H %M', '%m %d %y %H %M', '%B %d %y %H %M', '%b %d %y %H %M', '%b %d %Y', '%B %d %Y', '%m %d %Y', '%m %d %y', '%B %d %y', '%b %d %y', '%b %d %Y %H',
                   '%B %d %Y %H', '%m %d %Y %H', '%m %d %y %H', '%B %d %y %H', '%b %d %y %H', '%b %d %Y %H %M %S', '%B %d %Y %H %M %S', '%m %d %Y %H %M %S', '%m %d %y %H %M %S', '%B %d %y %H %M %S', '%b %d %y %H %M %S']

        dateValue = 'Error'

        for fmt in formats:
            try:
                dateValue = datetime.strptime(mydate, fmt)
            except ValueError as e:
                pass

        if dateValue is None:
            return 'Error'
        else:
            dateValue = str(dateValue)

        dateValue = dateValue.replace(':', '-')
        dateValue = dateValue.replace(' ', '-')
        dateValue = dateValue.split('-')

        year = (str(dateValue[0])).lstrip('0')
        month = (str(dateValue[1])).lstrip('0')
        day = (str(dateValue[2])).lstrip('0')
        hour = (str(dateValue[3])).lstrip('0')
        minute = (str(dateValue[4])).lstrip('0')
        second = (str(dateValue[5])).lstrip('0')

        while str(today.year) != year or str(today.month) != month or str(today.day) != day or str(today.hour) != hour or str(today.minute) != minute or str(today.second) != second:
            today = datetime.fromtimestamp(time.time())

        winsound.PlaySound('ding.wav', winsound.SND_FILENAME)

        return "Alarm Sounded"

    def notifyAfterXTime(self, x, unit='minutes'):
        unitlower = unit.lower()
        x = int(x)
        timenow = int(time.time())
        if unitlower == 'minutes':
            x = x * 60
        if unitlower == 'hours':
            x = x * 60 * 60

        while int(time.time()) != int(timenow + x):
            pass

        winsound.PlaySound('ding.wav', winsound.SND_FILENAME)

        return "Alarm Sounded"


class MyTest(unittest.TestCase):
    def test_me(self):
        a = AlarmClock()
        self.assertEqual(a.notifyAfterXTime(2, 'seconds'), ('Alarm Sounded'))  # OK
        self.assertEqual(a.notifyAtACertainTime('3/26/2017 11 19'), ('Alarm Sounded'))  # OK


unittest.main()
