#! python3
import unittest


class RomanNumerals:
    def __init__(self):
        self.numerals = [1000, 500, 100, 50, 10, 5, 1]
        self.numeralsdict = {1000: 'M',
                             500: 'D',
                             100: 'C',
                             50: 'L',
                             10: 'X',
                             5: 'V',
                             1: 'I'}

    def toRomanNumerals(self, number):
        romannumerals = ''
        for num in range(len(self.numerals)):
            numeral = self.numerals[num]
            nextnextnumeral = self.numerals[num + 2] if num + 2 < len(self.numerals) else None
            nextnumeral = self.numerals[num + 1] if num + 1 < len(self.numerals) else None
            while number - numeral >= 0:
                romannumerals += str(self.numeralsdict.get(numeral))
                number -= numeral
            if nextnextnumeral is not None:
                if self.numerals.index(nextnextnumeral) % 2 == 0:
                    if number - (numeral - nextnextnumeral) >= 0:
                        romannumerals += (str(self.numeralsdict.get(nextnextnumeral)) +
                                          str(self.numeralsdict.get(numeral)))
                        number -= (numeral - nextnextnumeral)
            if nextnumeral is not None:
                if self.numerals.index(nextnumeral) % 2 == 0 and self.numerals.index(numeral) % 2 == 1:
                    if number - (numeral - nextnumeral) >= 0:
                        romannumerals += (str(self.numeralsdict.get(nextnumeral)) +
                                          str(self.numeralsdict.get(numeral)))
                        number -= (numeral - nextnumeral)

        return romannumerals

    def toDecimal(self, string):
        number = 0
        for num in range(len(self.numerals)):
            numeral = self.numerals[num]
            nextnextnumeral = self.numerals[num + 2] if num + 2 < len(self.numerals) else None
            nextnumeral = self.numerals[num + 1] if num + 1 < len(self.numerals) else None
            numeralname = str(self.numeralsdict.get(numeral))
            nextnextnumeralname = str(self.numeralsdict.get(nextnextnumeral))
            nextnumeralname = str(self.numeralsdict.get(nextnumeral))
            for numberofnumeral in range(string.count(numeralname)):
                indexofnumeral = string.index(numeralname)
                if indexofnumeral - 1 >= 0:
                    if string[indexofnumeral - 1] == nextnextnumeralname:
                        number += (numeral - nextnextnumeral)
                        string = string.replace((nextnextnumeralname + numeralname), '')
                    if string[indexofnumeral - 1] == nextnumeralname:
                        number += (numeral - nextnumeral)
                        string = string.replace((nextnumeralname + numeralname), '')
                else:
                    number += numeral
                    string = string.replace(numeralname, '', 1)

        return number


class MyTest(unittest.TestCase):
    def test_me(self):
        r = RomanNumerals()
        self.assertEqual(r.toRomanNumerals(1515), ('MDXV'))  # OK
        self.assertEqual(r.toRomanNumerals(1998), ('MCMXCVIII'))  # OK
        self.assertEqual(r.toRomanNumerals(1414), ('MCDXIV'))  # OK
        self.assertEqual(r.toDecimal('MCMXCVIII'), (1998))  # OK
        self.assertEqual(r.toDecimal('MDXV'), (1515))  # OK
        self.assertEqual(r.toDecimal('MCDXIV'), (1414))  # OK


unittest.main()
