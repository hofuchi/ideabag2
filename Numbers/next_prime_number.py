#! python3
import unittest
# import msvcrt as wait


class PrimeNumber:
    def __init__(self):
        pass

    def nextPrimeNumber(self, startingnumber):
        for num in range(startingnumber + 1, 100000):
            numby = 0
            for number in range(2, num):
                if float(float(num) / float(number)).is_integer():
                    numby += 1
                    break
            if numby == 0:
                return num


class MyTest(unittest.TestCase):
    def test_me(self):
        p = PrimeNumber()
        self.assertEqual(p.nextPrimeNumber(7), (11))  # OK
        self.assertEqual(p.nextPrimeNumber(53), (59))  # OK


unittest.main()
