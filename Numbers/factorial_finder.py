#! python3
import unittest


class Factorial:
    def __init__(self):
        pass

    def factorialFinder(self, number):
        number = int(number)
        if number == 1:
            return 1
        else:
            return number * Factorial().factorialFinder(number - 1)


class MyTest(unittest.TestCase):
    def test_me(self):
        f = Factorial()
        self.assertEqual(f.factorialFinder(6), (720))  # OK


unittest.main()
