#! python3
import unittest


class LeastGreatestCommonDenominator:
    def __init__(self, num1, num2):
        self.num1 = float(num1)
        self.num2 = float(num2)

    def greatestCommonDenominator(self):
        if self.num2 > self.num1:
            for num in range(int(self.num2)):
                num = self.num2 - num
                if float(self.num2 / num).is_integer() and float(self.num1 / num).is_integer():
                    return num
        else:
            for num in range(int(self.num1)):
                num = self.num1 - num
                if float(self.num2 / num).is_integer() and float(self.num1 / num).is_integer():
                    return num

    def leastCommonDenominator(self):
        if self.num2 > self.num1:
            for num in range(int(self.num2)):
                num += 2
                if float(self.num2 / num).is_integer() and float(self.num1 / num).is_integer():
                    return num
        else:
            for num in range(int(self.num1)):
                num += 2
                if float(self.num2 / num).is_integer() and float(self.num1 / num).is_integer():
                    return num


class MyTest(unittest.TestCase):
    def test_me(self):
        lg = LeastGreatestCommonDenominator(9, 90)
        self.assertEqual(lg.leastCommonDenominator(), (3))  # OK
        self.assertEqual(lg.greatestCommonDenominator(), (9))  # OK


unittest.main()
