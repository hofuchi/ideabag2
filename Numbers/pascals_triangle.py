#! python3
import unittest


class PascalsTriangle:
    def __init__(self):
        pass

    def pascalsTriangle(self, rows):
        results = []
        resultstring = ''
        for n in range(rows):
            row = [1]
            if results:
                lastrow = results[-1]
                row.extend([sum(pair) for pair in zip(lastrow, lastrow[1:])])
                row.append(1)
            results.append(row)

        for num in range(len(results)):
            resultstring += str(results[num]) + '\n'

        return resultstring


class MyTest(unittest.TestCase):
    def test_me(self):
        p = PascalsTriangle()
        self.assertEqual(p.pascalsTriangle(
            6), ('[1]\n[1, 1]\n[1, 2, 1]\n[1, 3, 3, 1]\n[1, 4, 6, 4, 1]\n[1, 5, 10, 10, 5, 1]\n'))  # OK


unittest.main()
