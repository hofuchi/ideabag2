#! python3
import unittest
from math import pi


class Pi:
    def __init__(self):
        self.pi = str(pi)

    def findPiToTheNthDecimal(self, decimalplace):
        return int(self.pi[1 + decimalplace:decimalplace + 2])


class MyTest(unittest.TestCase):
    def test_me(self):
        p = Pi()
        self.assertEqual(p.findPiToTheNthDecimal(3), (1))  # OK
        self.assertEqual(p.findPiToTheNthDecimal(2), (4))  # OK


unittest.main()
