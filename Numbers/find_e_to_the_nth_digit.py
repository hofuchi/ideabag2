#! python3
import unittest


class EulersNumber:
    def __init__(self):
        self.e = '2.7182818284590452353602874713'

    def findEToTheNthDecimal(self, decimalplace):
        return int(self.e[1 + decimalplace:decimalplace + 2])


class MyTest(unittest.TestCase):
    def test_me(self):
        e = EulersNumber()
        self.assertEqual(e.findEToTheNthDecimal(3), (8))  # OK
        self.assertEqual(e.findEToTheNthDecimal(12), (9))  # OK


unittest.main()
