#! python3
import unittest
import math
from geopy.geocoders import Nominatim


class DistanceBetweenTwoCities:
    def __init__(self, city1, city2, unit="miles"):
        self.city1 = city1
        self.city2 = city2
        self.unit = unit
        self.geolocator = Nominatim()

    def coordinates(self):
        location1 = self.geolocator.geocode(self.city1)
        location2 = self.geolocator.geocode(self.city2)

        lat1, lng1 = location1.latitude, location1.longitude
        lat2, lng2 = location2.latitude, location2.longitude

        itey = str(lat1).split('.')
        beg = itey[0]
        end = itey[1]

        if '-' in beg:
            beg = beg[1:]
            beg += 's'
        else:
            beg += 'n'

        end = (int(end) * 60)
        lat1 = beg + str(end)

        itey = str(lng1).split('.')
        beg = itey[0]
        end = itey[1]

        if '-' in beg:
            beg = beg[1:]
            beg += 'w'
        else:
            beg += 'e'

        end = (int(end) * 60)
        lng1 = beg + str(end)

        itey = str(lat2).split('.')
        beg = itey[0]
        end = itey[1]

        if '-' in beg:
            beg = beg[1:]
            beg += 's'
        else:
            beg += 'n'

        end = (int(end) * 60)
        lat2 = beg + str(end)

        itey = str(lng2).split('.')
        beg = itey[0]
        end = itey[1]

        if '-' in beg:
            beg = beg[1:]
            beg += 'e'
        else:
            beg += 'w'

        end = (int(end) * 60)
        lng2 = beg + str(end)

        if 'n' in lat1:
            hr, mn = lat1.split('n')
            deg = float(hr) + float(mn) / 60
            lat_rad = math.radians(deg)
        elif 's' in lat1:
            hr, mn = lat1.split('s')
            deg = float(hr) + float(mn) / 60
            lat_rad = -math.radians(deg)
        if 'e' in lng1:
            hr, mn = lng1.split('e')
            deg = float(hr) + float(mn) / 60
            lng_rad = math.radians(deg)
        elif 'w' in lng1:
            hr, mn = lng1.split('w')
            deg = float(hr) + float(mn) / 60
            lng_rad = -math.radians(deg)
        if 'n' in lat2:
            hr, mn = lat2.split('n')
            deg = float(hr) + float(mn) / 60
            lat_rad2 = math.radians(deg)
        elif 's' in lat2:
            hr, mn = lat2.split('s')
            deg = float(hr) + float(mn) / 60
            lat_rad2 = -math.radians(deg)
        if 'e' in lng2:
            hr, mn = lng2.split('e')
            deg = float(hr) + float(mn) / 60
            lng_rad2 = math.radians(deg)
        elif 'w' in lng2:
            hr, mn = lng2.split('w')
            deg = float(hr) + float(mn) / 60
            lng_rad2 = -math.radians(deg)

        return (lat_rad, lng_rad, lat_rad2, lng_rad2)

    def findDistance(self):
        lat1, lng1, lat2, lng2 = DistanceBetweenTwoCities(self.city1, self.city2, self.unit).coordinates()
        kilolist = ["km", "kilometres", "kilometers"]

        if self.unit.lower() in kilolist:
            circ = 40075.2  # circumference in km measured at the equator
        else:
            circ = 24901.6  # circumference in miles measured at the equator

        a = lng1 - lng2

        if a < 0.0:
            a = -a
        if a > math.pi:
            a = 2.0 * math.pi - a

        angle = math.acos(math.sin(lat2) * math.sin(lat1) +
                          math.cos(lat2) * math.cos(lat1) * math.cos(a))
        distance = circ * angle / (2.0 * math.pi)

        return "%0.1f %s" % (distance, self.unit)


class MyTest(unittest.TestCase):
    def test_me(self):
        d = DistanceBetweenTwoCities("New York", "Paris")
        self.assertEqual(d.findDistance(), ("3939.3 miles"))  # OK
        d = DistanceBetweenTwoCities("New York", "Paris", "km")
        self.assertEqual(d.findDistance(), ("6339.7 km"))  # OK


unittest.main()
