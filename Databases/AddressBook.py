from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import *


class Database:
    def __init__(self):
        Base = declarative_base()
        self.db = create_engine('sqlite:///contacts.db')

    def initiateContactsDatabase(self):
        meta = MetaData(self.db)
        try:
            people = Table('people', meta, autoload=True)
        except Exception:
            people = Table('people', meta,
                           Column('name', String(44), primary_key=True),
                           Column('numbers', String(33)),
                           Column('emails', String(44)),
                           Column('notes', String(999)),)
            people.create()
        return people

    def commit(self):
        self.db.session.commit()


class Contacts:
    def __init__(self):
        self.people = Database().initiateContactsDatabase()
        self.i = self.people.insert()
        self.s = self.people.select

    def addContact(self, name, numbers=None, emails=None):
        self.i.execute(name=name, numbers=numbers, emails=emails)

    def removeContact(self, name):
        self.people.query.filter_by(name=name).delete()

    def getContact(self, name):
        s = self.s(self.people.c.name == name)
        rs = s.execute()
        row = rs.fetchone()
        print(row)

    def listContacts(self):
        s = self.s()
        rs = s.execute()
        for row in rs:
            print (row)


if __name__ == '__main__':
    Contacts().getContact('Juan')
